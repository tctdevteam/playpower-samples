import { NgModule } from '@angular/core';
import { PpCommonLibComponent } from './pp-common-lib.component';
import { PpHeaderComponent } from './pp-header/pp-header.component';
import { PpFooterComponent } from './pp-footer/pp-footer.component';
import { PpNavigationComponent } from './pp-navigation/pp-navigation.component';
import { PpButtonComponent } from './pp-button/pp-button.component';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { PpAlertComponent } from './pp-alert/pp-alert.component';
import { PpDialogComponent } from './pp-dialog/pp-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
  declarations: [PpCommonLibComponent, PpHeaderComponent,
     PpFooterComponent, PpNavigationComponent, PpButtonComponent,
      PpAlertComponent,
      PpDialogComponent],
  imports: [
    ButtonsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    NgModule,
    CommonModule,
    BrowserModule,
  ],
  exports: [PpCommonLibComponent, PpHeaderComponent,
     PpFooterComponent, PpNavigationComponent, PpButtonComponent,PpAlertComponent,PpDialogComponent]
})
export class PpCommonLibModule { }
