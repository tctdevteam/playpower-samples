import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpDialogComponent } from './pp-dialog.component';

describe('PpDialogComponent', () => {
  let component: PpDialogComponent;
  let fixture: ComponentFixture<PpDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
