import { Component, OnInit, Inject,NgModule } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogData } from '../model/confirm-dialog-data';

@Component({
  selector: 'lib-pp-dialog',
  templateUrl: './pp-dialog.component.html',
  styleUrls: ['./pp-dialog.component.css']
})
export class PpDialogComponent implements OnInit {

  message: string;
  title: string;
  confirmButton?: string;
  confirmButtonClass?: string;
  isConfirmDialog: boolean;

  constructor(public dialogRef: MatDialogRef<PpDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData) { }

  ngOnInit() {
    this.message = this.data.message;
    this.title = this.data.title;
    this.confirmButton = this.data.confirmButton;
    this.confirmButtonClass = this.data.confirmButtonClass;
  }

  delete() {
    this.data.confirmCallback();
  }

  cancel() {
    this.dialogRef.close();
  }

}
