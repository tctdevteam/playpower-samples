import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-pp-button',
  templateUrl: './pp-button.component.html',
  styleUrls: ['./pp-button.component.css']
})
export class PpButtonComponent implements OnInit {

  @Input() loading: boolean;
  @Input() icon: string;
  @Input() primary: boolean;

  constructor() { }

  ngOnInit() {
  }

  public onButtonClick() {

  }

}
