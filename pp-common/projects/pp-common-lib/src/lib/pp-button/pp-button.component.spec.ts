import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpButtonComponent } from './pp-button.component';

describe('PpButtonComponent', () => {
  let component: PpButtonComponent;
  let fixture: ComponentFixture<PpButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
