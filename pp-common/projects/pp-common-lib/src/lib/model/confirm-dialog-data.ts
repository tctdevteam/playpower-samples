export interface ConfirmDialogData {
  title: string;
  message: string;
  confirmButton: string;
  confirmButtonClass: string;
  confirmCallback: Function;
}
