import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpNavigationComponent } from './pp-navigation.component';

describe('PpNavigationComponent', () => {
  let component: PpNavigationComponent;
  let fixture: ComponentFixture<PpNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
