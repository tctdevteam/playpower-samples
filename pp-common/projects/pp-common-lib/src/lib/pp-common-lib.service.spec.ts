import { TestBed } from '@angular/core/testing';

import { PpCommonLibService } from './pp-common-lib.service';

describe('PpCommonLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PpCommonLibService = TestBed.get(PpCommonLibService);
    expect(service).toBeTruthy();
  });
});
