import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpCommonLibComponent } from './pp-common-lib.component';

describe('PpCommonLibComponent', () => {
  let component: PpCommonLibComponent;
  let fixture: ComponentFixture<PpCommonLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpCommonLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpCommonLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
