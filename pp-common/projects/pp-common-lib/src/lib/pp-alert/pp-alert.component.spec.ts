import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpAlertComponent } from './pp-alert.component';

describe('PpAlertComponent', () => {
  let component: PpAlertComponent;
  let fixture: ComponentFixture<PpAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
