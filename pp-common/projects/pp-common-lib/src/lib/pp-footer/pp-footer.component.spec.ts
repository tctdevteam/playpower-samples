import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpFooterComponent } from './pp-footer.component';

describe('PpFooterComponent', () => {
  let component: PpFooterComponent;
  let fixture: ComponentFixture<PpFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
