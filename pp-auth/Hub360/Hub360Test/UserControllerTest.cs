﻿using Hub360.Controllers;
using Hub360.Model;
using Hub360.Model.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using System.Linq;
using System.Threading;

namespace Hub360Test
{
    public class UserControllerTest
    {
        private UserController _userController;

        private readonly ITestOutputHelper _testOutputHelper;

        public UserControllerTest(ITestOutputHelper testOutputHelper)
        {
            _userController = new UserController();
            _testOutputHelper = testOutputHelper;
        }

        /// <summary>
        /// Test get user list.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task GetUsersTestAsync()
        {
            _userController.ControllerContext = Util.BuildControllerContext();

            var okResult = await _userController.GetAsync();

            // Assert
            var response = Assert.IsType<GetUsersResponseDataBlk>(okResult.Value);

            _testOutputHelper.WriteLine(Util.ToJSON(response));

            Assert.True(response.Users.Count > 0);
        }

        /// <summary>
        /// Test update user
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async System.Threading.Tasks.Task UpdateUsersTestAsync()
        {
            _userController.ControllerContext = Util.BuildControllerContext();

            string newName = "Test User";
            string id = "b17c44f5-ad05-46d3-bf61-bd7405936377";

            UserDataBlk request = new UserDataBlk
            {
                Id = id,
                Name = newName
            };

            var okResult = await _userController.UpdateUser(request);

            // Assert
            var response = Assert.IsType<Boolean>(okResult);
            Assert.True(response);

            var listResult = await _userController.GetAsync();
            var listResponse = Assert.IsType<GetUsersResponseDataBlk>(listResult.Value);

            var user = listResponse.Users.Where(u => u.Id.Equals(id)).SingleOrDefault();

            Assert.Equal(user.Name, newName);
        }


        [Fact]
        public async System.Threading.Tasks.Task DeleteUsersTestAsync()
        {
            _userController.ControllerContext = Util.BuildControllerContext();

            string id = "b17c44f5-ad05-46d3-bf61-bd7405936377";

            UserDataBlk request = new UserDataBlk
            {
                Id = id,
            };

            var okResult = await _userController.DeleteUser(request);

            // Assert
            var response = Assert.IsType<Boolean>(okResult);
            Assert.True(response);

            var listResult = await _userController.GetAsync();
            var listResponse = Assert.IsType<GetUsersResponseDataBlk>(listResult.Value);

            var user = listResponse.Users.Where(u => u.Id.Equals(id)).SingleOrDefault();

            Assert.Null(user);
        }
        [Fact]
        public async System.Threading.Tasks.Task VerifyToken()
        {
            string token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ilg1ZVhrNHh5b2pORnVtMWtsMll0djhkbE5QNC1jNTdkTzZRR1RWQndhTmsifQ.eyJleHAiOjE2MDA4MzM4NzUsIm5iZiI6MTYwMDgzMzU3NSwidmVyIjoiMS4wIiwiaXNzIjoiaHR0cHM6Ly9wbGF5cG93ZXJodWIzNjAuYjJjbG9naW4uY29tLzQxMmU0MDU3LTM0ZGYtNGU4YS1hZWUyLTFkYjBkOTVlN2Y3Ny92Mi4wLyIsInN1YiI6IjljMmNiNzI0LWQ3YWYtNDhjOS04YTZlLTE2ZWFiZDBiNDY4OCIsImF1ZCI6IjdlY2UxNzcxLTk2NzAtNDUyMy05MjI1LTIzMGY3YzYzOWEzZCIsIm5vbmNlIjoiMTIzNDUiLCJpYXQiOjE2MDA4MzM1NzUsImF1dGhfdGltZSI6MTYwMDgzMzUwMiwibmFtZSI6InNocnV0aGkiLCJnaXZlbl9uYW1lIjoic2hydXRoaSBzaHJ1dGhpIiwiZmFtaWx5X25hbWUiOiJzIiwidGZwIjoiQjJDXzFfU2lnblVwQW5kU2lnbkluIn0.haAC_-Dbe1nHWrecSalWLPquft9YaKkx0co56dNJlLDzv3XY__Fn47pIBiD0mLlr6_BCOWSqVoqH2TpmjAj_nvVDipsJnUDvdSVDZd33X3UbbL9FEFZJNtQLnLZUiGq7L_aF67dtB-zB-prHT9LsCcwSwHHsYM_oVX14srtJj-B1U7rASMIe3mQsvX0XTlu03PN5YuMA4wvkT79vzAFona4hghE9GwTsvoTzOkC44K72JpPaj8W38qARiQCJBQzpQhleZb9vWCoUPtEOt8dGPkI1HBkPvrUGqnuo30A54YKiFkc3DXs2RGWL4XmgO7gH7jrUYxKEhu5ltndLhWxSgw";
            _userController.ControllerContext = Util.BuildControllerContext();
            for (int i = 0; i<= 9;i++)
                {
                Thread.Sleep(120);
                _userController.VerifyToken(token);
                Assert.True(true);
                 }
        }
    }
}
