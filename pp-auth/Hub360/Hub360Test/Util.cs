﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using Xunit.Abstractions;

namespace Hub360Test
{
    public class Util
    {
        public static ControllerContext BuildControllerContext()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
             {
                new Claim(ClaimTypes.Name, "example name"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
             }, "mock"));

            ControllerContext context = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            return context;
        }

        public static string ToJSON(object obj)
        {
            string jsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return jsonStr;
        }
    }
}
