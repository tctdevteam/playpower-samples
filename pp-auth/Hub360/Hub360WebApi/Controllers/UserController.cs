﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Hub360WebApi.Controllers
{
    [RoutePrefix("api/Users")]
    public class UserController : ApiController
    {

        [HttpGet]
        [Route("Permissions")]
        public string[] GetPermissions()
        {
            string[] permissions = { "permission1", "permission2" };
            return permissions;
        }
    }
}
