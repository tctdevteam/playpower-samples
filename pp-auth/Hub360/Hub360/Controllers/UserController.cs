﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Hub360.Model;
using Hub360.Model.Response;
using Hub360.Service;
using IdentityModel;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace Hub360.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private UserService _userService;

        public UserController()
        {
            _userService = new UserService();
        }

        [HttpGet("")]
        public async Task<ActionResult<GetUsersResponseDataBlk>> GetAsync()
        {
            return await _userService.GetUsersAsync();
        }


        [HttpPost]
        [Route("update-user")]
        public async Task<Boolean> UpdateUser(UserDataBlk user)
        {
            return await _userService.UpdateUser(user);
        }

        [HttpPost]
        [Route("delete-user")]
        public async Task<Boolean> DeleteUser(UserDataBlk user)
        {
            return await _userService.DeleteUser(user);
        }

        [HttpPost]
        [Route("verify-token")]
        public async Task<ActionResult<ClaimsPrincipal>> VerifyToken(String token)
        {
            ClaimsPrincipal response = new ClaimsPrincipal();
            Debug.WriteLine("Main Method" + DateTime.Now);
            response = await ValidateJwt(token);
            Debug.WriteLine("Ending Time" + DateTime.Now);
            return new ClaimsPrincipal();
        }

        private async Task<ClaimsPrincipal> ValidateJwt(string jwt)
        {
            // read discovery document to find issuer and key material
            ClaimsPrincipal response = new ClaimsPrincipal();
            try
            {
                Debug.WriteLine("Starting Time" + DateTime.Now);
                var watch = new Stopwatch();

                watch.Start();
                string token = jwt;
                string myTenant ="d63a6e87 - 658e-4ba9 - 8464 - abc37ca43d0d";
                var myAudience = "6156f083-9e0a-483f-a339-551f4be5c10a";//application id or  client id
                var myIssuer = "https://playpowerhub360dev.b2clogin.com/412e4057-34df-4e8a-aee2-1db0d95e7f77/v2.0/";
                var stsDiscoveryEndpoint = "https://playpowerhub360dev.b2clogin.com/playpowerhub360dev.onmicrosoft.com/B2C_1_SignUpAndSignIn/v2.0/.well-known/openid-configuration";

                var configManager = new ConfigurationManager<OpenIdConnectConfiguration>(stsDiscoveryEndpoint, new OpenIdConnectConfigurationRetriever());
                var config = await configManager.GetConfigurationAsync();

                var tokenHandler = new JwtSecurityTokenHandler();

                var validationParameters = new TokenValidationParameters
                {
                    ValidAudience = myAudience,
                    ValidIssuer = myIssuer,
                    IssuerSigningKeys = config.SigningKeys,
                    ValidateLifetime = true
                };

                var validatedToken = (SecurityToken)new JwtSecurityToken();

                // Throws an Exception as the token is invalid (expired, invalid-formatted, etc.)  
                 tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
                
                Debug.WriteLine(validatedToken);
                Debug.WriteLine("Ending Time after sucess" + DateTime.Now);
                watch.Stop();
                Debug.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");
                return response;
            }
            
            catch (Exception e)
            {
                Console.WriteLine("Ending Time" + DateTime.Now);
                Console.WriteLine("Exption at verify user" + e.Message);
                return response;
            }
           
        }

        [HttpPost]
        [Route("create-user")]
        public async Task<Boolean> CreateUser(UserDataBlk request)
        {
            return await _userService.CreateUser(request);
            
        }
        [HttpGet]
        [Route("get-user")]
        public async Task<ActionResult<UserDataBlk>> GetUserById(string id)
        {
            return await _userService.GetUserById(id);
        }
        
    }
}