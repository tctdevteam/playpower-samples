﻿using Hub360.Model;
using Hub360.Model.Response;
using Microsoft.Graph;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hub360.Service
{
    public class UserService
    {
        public UserService()
        {

        }

        public async Task<GetUsersResponseDataBlk> GetUsersAsync()
        {
            ClientCredentialProvider authProvider = GetAuthProvider();
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);

            var messages = await graphClient.Users.Request().GetAsync();

            List<UserDataBlk> users = messages.ToList().Select(u => new UserDataBlk
            {
                Id = u.Id,
                Name = u.GivenName
            }).ToList();


            return new GetUsersResponseDataBlk
            {
                Users = users
            };
        }


        public ClientCredentialProvider GetAuthProvider()
        {
            IConfidentialClientApplication confidentialClientApplication = ConfidentialClientApplicationBuilder
                .Create("7ece1771-9670-4523-9225-230f7c639a3d")
                .WithTenantId("412e4057-34df-4e8a-aee2-1db0d95e7f77")
                .WithClientSecret("C-VT6e0-eQ9n90B_sT.On97ZsDEE23PlL0")
                .Build();

            ClientCredentialProvider authProvider = new ClientCredentialProvider(confidentialClientApplication);
            return authProvider;
        }

        public async Task<Boolean> UpdateUser(UserDataBlk userToUp)
        {
            ClientCredentialProvider authProvider = GetAuthProvider();
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);
            try
            {

                var user = new User
                {
                    Id = userToUp.Id,
                    GivenName = userToUp.Name
                };

                await graphClient.Users[userToUp.Id]
               .Request()
               .UpdateAsync(user);
            }
            catch(Exception e)
            {
                return false;
            }
            return true;

        }

        public async Task<Boolean> DeleteUser(UserDataBlk userToDelete)
        {
            ClientCredentialProvider authProvider = GetAuthProvider();
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);
            try
            {
                await graphClient.Users[userToDelete.Id]
                 .Request()
                 .DeleteAsync();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public async Task<Boolean> CreateUser(UserDataBlk request)
        {
            ClientCredentialProvider authProvider = GetAuthProvider();
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);
            try
            {
                var user = new User
                {
                    AccountEnabled = true,
                    DisplayName = "Chiru",
                    MailNickname = "Chiru",
                    UserPrincipalName = "jobseeker.chiru@gmail.com",
                    GivenName = "Chiru",
                    PasswordProfile = new PasswordProfile
                    {
                        ForceChangePasswordNextSignIn = true,
                        Password = "password-value"
                    }
                };

                await graphClient.Users
                    .Request()
                    .AddAsync(user);
                return true;
            }
           catch(Exception e)
            {
                return false;
            }
        }

        public async Task<UserDataBlk> GetUserById(string id)
        {
            ClientCredentialProvider authProvider = GetAuthProvider();
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);
            UserDataBlk datablk = new UserDataBlk();
            try
            {
                var response = await graphClient.Users[id]
                 .Request().GetAsync();
                datablk.Id = response.Id;
                datablk.Name = response.GivenName;
                
                return datablk;
            }
            catch (Exception e)
            {
                return datablk;
            }

        }
    }
}
