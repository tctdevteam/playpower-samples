﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hub360.Model.Response
{
    public class GetUsersResponseDataBlk
    {

        public List<UserDataBlk> Users { get; set; }
    }
}
