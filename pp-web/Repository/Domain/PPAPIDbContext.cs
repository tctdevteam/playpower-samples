﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PPSample.Migrations;

namespace PPSample.Repository.Domain
{
    public class PPAPIDbContext: IdentityDbContext<User, IdentityRole, 
        string,IdentityUserLogin,IdentityUserRole,IdentityUserClaim>
    {
        public PPAPIDbContext()
          : base("PPDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<PPAPIDbContext, Migrations.Configuration>("PPDbContext"));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("PPUser");
        }
}