﻿using PPSample.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace PPSample.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PPAPIDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PPAPIDbContext context)
        {

        }
    }
}