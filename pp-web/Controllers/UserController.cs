﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PPSample.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PPSample.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        private UserManager<User> _userManager;
        private readonly SignInManager<User,string> _signInManager;

        public UserController(UserManager<User> userManager, SignInManager<User, string> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
       
    }
}