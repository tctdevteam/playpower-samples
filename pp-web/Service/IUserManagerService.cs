﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPSample.Service
{
    public interface IUserManagerService
    {
         Boolean AddUser();
         void GetUserList();
    }
}