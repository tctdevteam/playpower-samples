﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PPSample.Models.Request
{
    public class RegisterUserRequestDataBlk
    {
        [Required]
        [Display(Name = "Email address")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

    }
}