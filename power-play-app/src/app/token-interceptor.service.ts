import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../app/auth/auth.service';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  private refreshTokenInProgress = false;
    // Refresh Token Subject tracks the current token, or is null if no token is currently
    // available (e.g. refresh pending).

  constructor(private authService:AuthService
    ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.authService.getToken();
    let tokenizedReq = req.clone({
      setHeaders:{
        Authorization: 'Beared '+ token,
      }
    });
  //   return next.handle(req).catch(err => {
  //     console.log(err);
  //     if (err.status === 401) {

  //             //Genrate params for token refreshing
  //           let params = {
  //             token: token,
  //             refreshToken: this.authService.getRefreshToken()
  //           };
  //           return this.http.post('localhost:8080/auth/refresh', params).flatMap(
  //             (data: any) => {
  //               //If reload successful update tokens
  //               if (data.status == 200) {
  //                 //Update tokens
  //                 localStorange.setItem("api-token", data.result.token);
  //                 localStorange.setItem("refreshToken", data.result.refreshToken);
  //                 //Clone our fieled request ant try to resend it
  //                 req = req.clone({
  //                   setHeaders: {
  //                     'api-token': data.result.token
  //                   }
  //                 });
  //                 return next.handle(req).catch(err => {
  //                   //Catch another error
  //                 });

  //             }
  //           );
  //         }else {
  //             //Logout from account or do some other stuff
  //         }
  //     }
  //     return Observable.throw(err);
  // });

    return next.handle(tokenizedReq);
  }

}
