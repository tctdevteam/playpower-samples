import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { UserManagementModule } from '../app/user-management/user-management.module';
import { PpCommonModule } from '../app/pp-common/pp-common.module';
import { DashboardModule } from '../app/dashboard/dashboard.module';
//import { PpCommonLibModule } from 'pp-common-lib';
import { AuthModule } from '../app/auth/auth.module';
import { TokenInterceptorService } from '../app/token-interceptor.service';
import {AuthService } from '../app/auth/auth.service';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GridModule,
    BrowserAnimationsModule,
    ToastrModule,
    RxReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule,
    UserManagementModule,
    PpCommonModule,
    DashboardModule,
    AuthModule
  ],
  providers: [AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass:TokenInterceptorService,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
