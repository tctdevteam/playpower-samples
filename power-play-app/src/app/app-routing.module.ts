import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Path} from '../app/constant/path';
import { LoginComponent } from '../app/user-management/login/login.component';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { UserListComponent } from '../app/user-management/user-list/user-list.component';

const routes: Routes = [
  {
    path: Path.Login,
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: Path.Dashboard,
    component: DashboardComponent,
    data: {
      title: 'Dashboard'
    },
    children: [
        {
          path: Path.UserList,
          component: UserListComponent,
          data: {
            title: 'User List'
          }
      }],
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
