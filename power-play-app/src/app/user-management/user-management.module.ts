import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserListComponent } from './user-list/user-list.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule } from '@progress/kendo-angular-grid';
import { FormsModule } from '@angular/forms';
import {  ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { AppRoutingModule } from '../app-routing.module';
// import { PpCommonLibModule } from 'pp-common-lib';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, UserListComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    GridModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    ButtonsModule,
    AppRoutingModule
  ],
  exports:[]
})
export class UserManagementModule {


}
