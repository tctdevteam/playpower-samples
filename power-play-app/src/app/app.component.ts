import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Path } from './constant/path';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'power-play-app';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    let url = new URL(window.location.href);
    let token = url.searchParams.get('accessToken');
    if (token) {
      // insert verify token from server code
      localStorage.setItem('token', token);
      this.router.navigateByUrl(Path.Dashboard);
    }
    else {
      window.location.href = environment.hub360BaseUrl + '?redirectUrl=' + window.location.href;
      // const stoken = localStorage.getItem('token');
      // if (stoken) {
      //   this.router.navigateByUrl(Path.Dashboard);
      // }
      // else {
      //   window.location.href = environment.hub360BaseUrl + '?redirectUrl=' + window.location.href;
      // }
    }
  }
}
