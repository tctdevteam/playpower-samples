import { Component, OnInit } from '@angular/core';
import { PpHeaderComponent } from './pp-header/pp-header.component';
import { PpFooterComponent } from './pp-footer/pp-footer.component';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Path } from '../constant/path';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  returnToHub() {
    window.location.href = environment.hub360BaseUrl;
  }

  logout() {
    this.authService.logout();
  }
}
