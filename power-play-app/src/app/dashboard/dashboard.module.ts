import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PpHeaderComponent } from './pp-header/pp-header.component';
import { PpFooterComponent } from './pp-footer/pp-footer.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [PpHeaderComponent, PpFooterComponent,DashboardComponent],
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule
  ]
})
export class DashboardModule { }
