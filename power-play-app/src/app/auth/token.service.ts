import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Cons } from '../constant/cons';
@Injectable({
  providedIn: 'root'
})
export class TokenService extends ApiService {
  baseUrl = this.baseWebApiUrl + 'api/token';
  constructor(private http: HttpClient) {
    super();
  }

  getToken(reqData: any): Observable<any> {
    return this.http.post(this.baseUrl + 'token', reqData);
   //return Cons.Token;
  }

}
