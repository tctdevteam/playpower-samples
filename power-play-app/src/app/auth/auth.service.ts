import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  // tslint:disable-next-line: typedef
  getToken() {
    return localStorage.getItem('token');
  }
  // tslint:disable-next-line: typedef
  getRefreshToken() {
    return localStorage.getItem('refreshToken');
  }
  // tslint:disable-next-line: typedef
  setToken(token) {
    localStorage.setItem('token', token);
  }
  // tslint:disable-next-line: typedef
  setRefreshToken(refreshToken) {
    localStorage.setItem('refreshToken', refreshToken);
  }
// tslint:disable-next-line: typedef
  public logout() {
    localStorage.clear();
    window.location.href = environment.hub360LogoutUrl;
  }
}
