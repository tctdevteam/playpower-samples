import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ApiService{

  protected baseWebApiUrl = environment.baseWebApiUrl;
  httpOptions = httpOptions;

  constructor() {

  }
}
