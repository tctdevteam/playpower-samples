// Custom JavaScript
$( document ).ready(function() {
	
	if($('#projects-datatable').length){
		var projectstable = $('#projects-datatable').DataTable({
			columns: [
				{ data: 'Action' },
				{ data: 'Project' },
				{ data: 'Title' },
				{ data: 'Doc #', visible: false },
				{ data: 'Customer' },
				{ data: 'Sales Rep' },
				{ data: 'Date', visible: false },
				{ data: 'Exp. Close', visible: false },
				{ data: 'Status', visible: false },
				{ data: 'Probability', visible: false },
				{ data: 'Forecast Type', visible: false },
				{ data: 'Projected Total'},
				{ data: 'Exp. Install', visible: false },
				{ data: 'Payment Terms', visible: false }
			],
			"oLanguage": {
				"sSearch": "Filter Table"
			}
		});

		// For DataTable Column Toggle
		$('input.toggle-vis').on( 'change', function (e) {
			e.preventDefault();

			// Get the column API object
			var column = projectstable.column( $(this).attr('data-column') );

			// Toggle the visibility
			column.visible( ! column.visible() );
		} );
	} // END if projects datatable
	
	// Example Validation
	if($('input#project-date').length) {	
		$('input#project-date').blur(function() {
			var formField = $(this);
			var fieldFeedback = $(this).next();
			
			if( !this.value ) {
				formField.removeClass('is-valid').addClass('is-invalid');
				fieldFeedback.show();
			} else {
				formField.removeClass('is-invalid').addClass('is-valid');
				fieldFeedback.hide();
			}
		});	
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	$('.toast').toast();			  
	setTimeout(function() {
			$('#toast-login').toast('show');
	}, 1000);
	
	$('#toast-login').on('hidden.bs.toast', function () {
		setTimeout(function() {
			$('#toast-cool').toast('show');
		}, 500);
		setTimeout(function() {
			$('#toast-cool2').toast('show');
		}, 3500);
	});
	
	if($('#toast-saved').length){
		setTimeout(function() {
			$('#toast-saved').toast('show');
		}, 1500);
	}
	
	if($('#toast-logout').length){
		setTimeout(function() {
			$('#toast-logout').toast('show');
		}, 2000);
	}
	
});
