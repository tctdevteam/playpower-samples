export class GenericResponse {
  public IsSuccess: boolean;
  public Message: string;
  public DetailedMessage: string;
  public StatusCode: number;
}
