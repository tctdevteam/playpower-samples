import { UserDataBlk } from '../user-datablk';
import {
  prop, propObject
} from '@rxweb/reactive-form-validators';
export class GetUserListResponseDataBlk
{
  users: [UserDataBlk];
  constructor(obj)
  {
    if(obj)
    {
    this.users = obj.users;
    }
  }
}
