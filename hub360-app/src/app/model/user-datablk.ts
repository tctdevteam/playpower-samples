import {
  prop, propObject } from '@rxweb/reactive-form-validators';
export class UserDataBlk
{
  @prop()
  Id: string;
  @prop()
  Name: string;
  constructor(obj)
  {
    if(obj !=null)
    {
      this.Id = obj.Id;
      this.Name = obj.Name;
    }
  }
}
