import {
  required, compare, maxLength, lessThanEqualTo,
  time, date, prop, propObject
} from '@rxweb/reactive-form-validators';
export class LoginRequestDataBlk
{
  @prop()
  public Username: string;
  @prop()
  public Password: string;

  constructor(obj)
  {
    if(obj != null)
    {
      this.Username= obj.username;
      this.Password = obj.Password;
    }
    else
    {
      this.Username = null;
      this.Password = null;
    }
  }
}
