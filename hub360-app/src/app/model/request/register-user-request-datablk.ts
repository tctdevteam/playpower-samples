export class RegisterUserRequestDataBlk {
  EmailAddress: string;
  UserName: string;
  FirstName: string;
  LastName: string;
}
