import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  getToken()
  {
    return  localStorage.getItem('token');
    //localStorange.setItem("refreshToken", data.result.refreshToken);
  }

  getRefreshToken()
  {
    return localStorage.getItem('refreshToken');
  }
  setToken(tokenstring:string)
  {
    localStorage.setItem('token',tokenstring);
  }
  setRefreshToken(refreshTokenString:string)
  {
    localStorage.setItem('refreshToken',refreshTokenString);
  }
}
