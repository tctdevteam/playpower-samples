import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public http: HttpClient,
    private router: Router) { }

  public logout() {
    localStorage.clear();
    window.location.href = environment.b2cloginUrl;
  }
}
