import { Component, OnInit } from '@angular/core';
import {LoginRequestDataBlk } from '../../model/index';
import { FormGroup } from '@angular/forms';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { Router } from '@angular/router';
import { Path } from 'src/app/constant/path';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  path = Path;
  loginRequestData: LoginRequestDataBlk;
  loginRequestFormGroup :FormGroup;
  errorMessage: string;
  loading: boolean = false;

  constructor(
     private router: Router,
     private formBuilder: RxFormBuilder) { }

  ngOnInit() {


    this.loginRequestData = new LoginRequestDataBlk(null);
    this.loginRequestFormGroup = this.formBuilder.formGroup(this.loginRequestData);
    this.errorMessage = null;
  }

  login(url)
  {
    this.router.navigateByUrl(url);
  }

}
