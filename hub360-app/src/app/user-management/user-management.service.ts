import { Injectable } from '@angular/core';
import { ApiService } from './../api/api.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserManagementService extends ApiService{

  private webBaseUrl = this.baseWebApiUrl;
  private usersUrl = this.webBaseUrl +'User';

  constructor(private _http: HttpClient) {
    super();
  }

  updateUser(reqDataBlk: any): Observable<any> {
    return this._http.post(this.webBaseUrl + '/update-user', reqDataBlk);
  }

  getUsers(): Observable<any>
  {
    return this._http.get(this.usersUrl);
  }
  deleteUser(reqDataBlk :any):Observable<any>{
    return this._http.post(this.webBaseUrl + '/delete',reqDataBlk);
  }
}
