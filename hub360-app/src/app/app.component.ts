import { Component } from '@angular/core';
import { TokenService } from '../app/auth/token.service';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Path } from './constant/path';
import { b2cPolicies, isIE } from './app-config';
import { CryptoUtils, Logger } from 'msal';
import { BroadcastService, MsalService } from '@azure/msal-angular';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Azure AD B2C';
  isIframe = false;
  loggedIn = false;

  constructor(private broadcastService: BroadcastService,
    private authService: MsalService,
    private router: Router,
    private tokenService: TokenService) { }

  //ngOnInit() {


  //   this.isIframe = window !== window.parent && !window.opener;
  //   this.checkAccount();

  //   // event listeners for authentication status
  //   this.broadcastService.subscribe('msal:loginSuccess', (success) => {

  //   // We need to reject id tokens that were not issued with the default sign-in policy.
  //   // "acr" claim in the token tells us what policy is used (NOTE: for new policies (v2.0), use "tfp" instead of "acr")
  //   // To learn more about b2c tokens, visit https://docs.microsoft.com/en-us/azure/active-directory-b2c/tokens-overview
  //     if (success.idToken.claims.acr === b2cPolicies.names.resetPassword) {
  //       window.alert('Password has been reset successfully. \nPlease sign-in with your new password');
  //       return this.authService.logout();
  //     }

  //     console.log('login succeeded. id token acquired at: ' + new Date().toString());
  //     console.log(success);

  //     this.checkAccount();
  //   });

  //   this.broadcastService.subscribe('msal:loginFailure', (error) => {
  //     console.log('login failed');
  //     console.log(error);

  //       // Check for forgot password error
  //       // Learn more about AAD error codes at https://docs.microsoft.com/en-us/azure/active-directory/develop/reference-aadsts-error-codes
  //     if (error.errorMessage.indexOf('AADB2C90118') > -1) {

  //           this.authService.loginRedirect(b2cPolicies.authorities.resetPassword);

  //       }
  //   });

  //   // redirect callback for redirect flow (IE)
  //   this.authService.handleRedirectCallback((authError, response) => {
  //     if (authError) {
  //       console.error('Redirect Error: ', authError.errorMessage);
  //       return;
  //     }

  //     console.log('Redirect Success: ', response);
  //   });

  //   this.authService.setLogger(new Logger((logLevel, message, piiEnabled) => {
  //     console.log('MSAL Logging: ', message);
  //   }, {
  //     correlationId: CryptoUtils.createNewGuid(),
  //     piiLoggingEnabled: false
  //   }));
  // }

  // // other methods
  // checkAccount() {
  //   this.loggedIn = !!this.authService.getAccount();
  //   this.router.navigateByUrl(Path.Dashboard);
  // }

  // login() {
  //     this.authService.loginRedirect();
  // }

  // logout() {
  //   this.authService.logout();
  //   this.login();
  // }

  // editProfile() {
  //     this.authService.loginRedirect(b2cPolicies.authorities.editProfile);
  // }

  // constructor(private tokenService:TokenService,
  //   private router:Router)
  // {

  // }
  // }
  ngOnInit() {


  }
}
