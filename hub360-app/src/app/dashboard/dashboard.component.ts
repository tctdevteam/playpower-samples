import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { CryptoUtils, InteractionRequiredAuthError, Logger } from 'msal';
import { environment } from 'src/environments/environment';
import { b2cPolicies,apiConfig } from '../app-config';
import { Path } from '../constant/path';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  title = 'Azure AD B2C';
  isIframe = false;
  loggedIn = false;
  profile :any;
  redirectUri: string;
  accessToken:any;


  constructor(private broadcastService: BroadcastService,
    private authService: MsalService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient) { }

  ngOnInit(): void {
    this.loggedIn = !!this.authService.getAccount();
    this.redirectUri = null;

    this.checkRoute();
   // this.checkAccount();
    this.isIframe = window !== window.parent && !window.opener;

    // event listeners for authentication status
    this.broadcastService.subscribe('msal:loginSuccess', (success) => {

      if(success && success.idTokenClaims)
      {
        this.profile = success.idTokenClaims;
      }
      if(success.accountState != 'null') {
        const state = JSON.parse(success.accountState);
        this.redirectUri = state['redirect'];
      }


      // We need to reject id tokens that were not issued with the default sign-in policy.
      // "acr" claim in the token tells us what policy is used (NOTE: for new policies (v2.0), use "tfp" instead of "acr")
      // To learn more about b2c tokens, visit https://docs.microsoft.com/en-us/azure/active-directory-b2c/tokens-overview
      if (success.idToken.claims.acr === b2cPolicies.names.resetPassword) {
        window.alert('Password has been reset successfully. \nPlease sign-in with your new password');
        return this.authService.logout();
      }

      console.log('login succeeded. id token acquired at: ' + new Date().toString());
      console.log(success);

      this.checkAccount();
    });

    this.broadcastService.subscribe('msal:loginFailure', (error) => {
      console.log('login failed');
      console.log(error);

      // Check for forgot password error
      // Learn more about AAD error codes at https://docs.microsoft.com/en-us/azure/active-directory/develop/reference-aadsts-error-codes
      if (error.errorMessage.indexOf('AADB2C90118') > -1) {

        this.authService.loginRedirect(b2cPolicies.authorities.resetPassword);

      }
    });

    // redirect callback for redirect flow (IE)
    this.authService.handleRedirectCallback((authError, response) => {
      if (authError) {
        console.error('Redirect Error: ', authError.errorMessage);
        return;
      }

      console.log('Redirect Success: ', response);
    });

    this.authService.setLogger(new Logger((logLevel, message, piiEnabled) => {
      console.log('MSAL Logging: ', message);
    }, {
      correlationId: CryptoUtils.createNewGuid(),
      piiLoggingEnabled: false
    }));
  }

  checkRoute() {
      this.route.queryParams.subscribe(params => {
        this.redirectUri = params['redirectUrl'];
        this.checkAccount();
      });

  }

  // other methods
  checkAccount() {
    this.loggedIn = !!this.authService.getAccount();
    if(!this.loggedIn)
    {
      this.login();
    }
    else{
      this.accessToken = localStorage.getItem("msal.idtoken");
      //if the request from ROE application
      if(this.redirectUri && this.redirectUri !== "null")
      {
          this.redirectreo();
      }
    }
  }

  login() {
    this.authService.loginRedirect({
      state: this.redirectUri ? '{redirect: ' + this.redirectUri  +'}' : 'null'
    });
  }

  logout() {
    this.router.navigateByUrl(Path.Logout);
  }

  editProfile() {
    this.authService.loginRedirect(b2cPolicies.authorities.editProfile);
  }

  redirectreo()
  {
    if(this.redirectUri && this.redirectUri !== "null")
    {
      window.location.href = this.redirectUri + '?accessToken=' + this.accessToken;
    }
    else{
      window.location.href = environment.roeBaseUrl + '?accessToken=' + this.accessToken;
    }
  }

}
