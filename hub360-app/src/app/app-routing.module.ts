import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';
import { Path } from './constant/path';
import { UserListComponent } from './user-management/user-list/user-list.component';
import { UserProfileComponent } from './user-management/user-profile/user-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserLogoutComponent } from './user-management/user-logout/user-logout.component';
const routes: Routes = [
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [
      MsalGuard
    ]
  },
  {
    path:Path.Dashboard,
    component: DashboardComponent,
    data:{
      title:'Dashboard'
    }
  },
  {
    path: Path.Logout,
    component: UserLogoutComponent,
    data: {
      title: 'Logout'
    }
  },
  {
          path: Path.UserList,
          component: UserListComponent,
          data: {
            title: 'User List'
          }
  }
 // ,{
    // path: Path.Dashboard,
    // component: DashboardComponent,
    // data: {
    //   title: 'Dashboard'
    // }
    // ,children: [
    //     {
    //       path: Path.UserList,
    //       component: UserListComponent,
    //       data: {
    //         title: 'User List'
    //       }
    //   }],
    //},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
