import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenService } from './auth/token.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

   constructor(
   private tokenService:TokenService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // const token: string = this.tokenService.getToken();

    // let tokenizedReq = request.clone({
    //   setHeaders:{
    //    // Authorization: 'Beared '+ token,
    //   }
    // })
    // return next.handle(tokenizedReq);

    return next.handle(request);
  }
}
