// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  b2cloginUrl:'https://playpowerhub360.b2clogin.com/playpowerhub360.onmicrosoft.com/oauth2/v2.0/authorize?p=b2c_1_signupandsignin&client_id=7ece1771-9670-4523-9225-230f7c639a3d&response_type=code+id_token&response_mode=query&scope=openid%20offline_access&state=arbitrary_data_you_can_receive_in_the_response&nonce=12345&redirect_uri=http://localhost:4200',
  baseServerUrl:'https://localhost:44370/api/',
  roeBaseUrl:'http://localhost:4201/OE-home'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
